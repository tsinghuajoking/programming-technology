<h1 align="center"> 使用soundcard在Python中操作声卡 </h1>
<br>


<p align="center"><img src="https://img-blog.csdnimg.cn/71bd3bce976b4d0b931135aeaf6841bd.png" width="800" /></p>


><font  face=黑体 color=purple size=4>简 介：</font> 利用Python中的soundcard软件包可以对声卡的MIC，SPEAKER进行操作。基于此，配合可编程信号源DG1062可以获得声卡的详细的幅频特性。
<br><br><font color=slateblue face=宋体>**``关键词``**：</font> **声卡**，**soundcard**，**dg1062**，**幅频特性**





&nbsp;
<h1 align="center"> §<center><font  face=黑体 color=slateblue size=6><u>01</u> <font color=red>操</font>作声卡</font></center> </h1>

---




&emsp;&emsp;<font color=gray size=5>在</font>之前通过实验（ [**测试计算机声卡双通道录音的频率特性**](https://zhuoqing.blog.csdn.net/article/details/120874310) ）初步测试了计算机声卡录制声音所对应的频率范围，并在  [**双声道录制的混动波形信号**](https://zhuoqing.blog.csdn.net/article/details/120852245) 记录了 [**混沌电路产生的混沌物理信号**](https://zhuoqing.blog.csdn.net/article/details/120557306) 。实验中使用了录音软件Audacity进行录制。为了方便后期实验，需要测试直接在Python语言中操作声卡。

## <font  color=#7a37ab>一、soundcard 软件包</font>

&emsp;&emsp;在python中提供对于声卡操作的软件包包括：
*  [**PySoundCard**](https://pypi.org/project/PySoundCard/) 
*  [**SoundCard 0.4.1**](https://pypi.org/project/SoundCard/) 


&emsp;&emsp;上面两个软件都可以正常安装。由于只在 [**SoundCard 0.4.1**](https://pypi.org/project/SoundCard/) 中给出了示例程序，所以下面仅对**SoundCard**软件包进行测试。

### <font  color=#008090>1、读取音频设备信息</font>
#### <font  color=#e06030>(1) 测试程序</font>
```python
from headm import *
import soundcard as sc

speakers = sc.all_speakers()
printf(speakers)
default_speaker = sc.default_speaker()
printf(default_speaker)
mics = sc.all_microphones()
printf(mics)
default_mic = sc.default_microphone()
printf(default_mic)
```

#### <font  color=#e06030>(2) 读取结果</font>
```python
[<Speaker 扬声器 (VIA High Definition Audio) (2 channels)>, <Speaker DELL U2410-1 (NVIDIA High Definition Audio) (2 channels)>, <Speaker SPDIF Interface (TX0) (VIA High Definition Audio) (2 channels)>, <Speaker SPDIF Interface (TX1) (VIA High Definition Audio) (2 channels)>]

<Speaker 扬声器 (VIA High Definition Audio) (2 channels)>

[<Microphone Analog (USB Capture AIO Analog) (2 channels)>, <Microphone 立体声混音 (VIA High Definition Audio) (2 channels)>, <Microphone Digital (USB Capture AIO) (2 channels)>, <Microphone 麦克风 (3- USB Audio Device) (1 channels)>, <Microphone 数字音频接口 (USB3. 0 capture) (1 channels)>, <Microphone 线路输入 (VIA High Definition Audio) (2 channels)>]

<Microphone 麦克风 (3- USB Audio Device) (1 channels)>
```

### <font  color=#008090>2、获取特定输入</font>


&emsp;&emsp;利用 ``get_microphone()`` 获得对应的``mic``输入。
```python
one_mic = sc.get_microphone('线路输入')
printf(one_mic)
```


&emsp;&emsp;上述代码输出：


```python
<Microphone 线路输入 (VIA High Definition Audio) (2 channels)>
```

## <font  color=#7a37ab>二、录音与回复</font>

### <font  color=#008090>1、录音</font>


&emsp;&emsp;下面给出了录音的函数：

```python
data = default_mic.record(samplerate=48000, numframes=48000)
```

### <font  color=#008090>2、回放函数</font>

&emsp;&emsp;下面给出了回放函数：

```python
default_speaker.play(data/numpy.max(data), samplerate=48000)
```

### <font  color=#008090>3、录音和回复</font>


&emsp;&emsp;下面的代码测试了利用 “**线路输入**”进行录音，然后通过缺省喇叭进行回放。


```python
with one_mic.recorder(samplerate=48000) as mic,\
     default_speaker.player(samplerate=48000) as sp:
    for _ in range(10):
        data = mic.record(numframes=10240)
        sp.play(data)
```



&emsp;&emsp;修改回复的samplerate参数，可以改变回复是声音的频率。

## <font  color=#7a37ab>三、显示录制信号波形</font>
### <font  color=#008090>1、采集正弦波</font>
```python
from headm import *
import soundcard as sc

default_mic = sc.default_microphone()

one_mic = sc.get_microphone('线路输入')

data = one_mic.record(samplerate=48000, numframes=1024)
plt.plot(data)
plt.xlabel("Samples")
plt.ylabel("Values")
plt.grid(True)
plt.tight_layout()
plt.show()
```
<p align="center"><img src="https://img-blog.csdnimg.cn/c5dcafacc34844eab5420ae52d82c5cf.png" width="640" /><div align="center">▲ 图1.3.1 显示采集到的正弦波波形</div></p><br>


<p align="center"><img src="https://img-blog.csdnimg.cn/ea6214f3841f440b93b7a5d9f659a226.png" width="640" /><div align="center">▲ 图1.3.2 显示采集到的正弦波波形</div></p><br>



&emsp;&emsp;从上面可以看到在数据的起始部分显示了一些数据为0的采集数据。这可能是声卡在被初始化的时候对应的起始数据为0。下面通过对声卡先进行初始化，采集 一段之后，再采集，就可以避免这种情况。

```python
from headm import *
import soundcard as sc

default_mic = sc.default_microphone()

one_mic = sc.get_microphone('线路输入')

with one_mic.recorder(samplerate=48000) as mic:
    mic.record(numframes=1000)
    data = mic.record(numframes=1024)
    plt.plot(data)
    plt.xlabel("Samples")
    plt.ylabel("Values")
    plt.grid(True)
    plt.tight_layout()
    plt.show()
```
<p align="center"><img src="https://img-blog.csdnimg.cn/1d6757a064304da588c17ac410d19c33.png" width="640" /><div align="center">▲ 图1.3.3 显示采集到的1kHz的声音波形</div></p><br>


&nbsp;
<h1 align="center"> §<center><font  face=黑体 color=slateblue size=6><u>02</u> <font color=red>声</font>卡带宽</font></center> </h1>

---




&emsp;&emsp;<font color=gray size=5>在</font> [**测试计算机声卡双通道录音的频率特性**](https://zhuoqing.blog.csdn.net/article/details/120874310) 对于声卡的带宽进行了初步测试。下面通过Python来对声卡采集到的数据进行处理，便可以获得多点不同频率下的声卡采集到的信号的幅度，进而可以更加精确获得声卡的带宽。

## <font  color=#7a37ab>一、测量方法</font>

&emsp;&emsp;利用 [**DG1062可编程信号源**](https://zhuoqing.blog.csdn.net/article/details/110824422) 产生不同频率下的信号，送到声卡进行采集。通过读取声卡采集到的信号，计算出它对应的幅值（通过对最大值，最小值进行检测）进而计算采集到的信号的幅值。


&emsp;&emsp;通过测量一组不同频率下的声卡获得信号的幅值，绘制出声卡的幅频特性。


## <font  color=#7a37ab>二、测量结果</font>


### <font  color=#008090>1、测量低频截止频率</font>

#### <font  color=#e06030>(1) 测量代码</font>

```python
from headm import *
import soundcard as sc
from tsmodule.tsvisa        import *

dg1062open(103)
dg1062freq(1,2000)

one_mic = sc.get_microphone('线路输入')

pltgif = PlotGIF()
with one_mic.recorder(samplerate=48000) as mic:
    mic.record(numframes=100000)

    fdim = linspace(1, 10, 50)
    ddim = []
    for f in fdim:
        dg1062freq(1,f)
        data = mic.record(numframes=48000)

        damp = max(list(data[:,0])) - min(list(data[:,0]))
        ddim.append(damp)

        printff(f, damp)

        plt.clf()
        plt.plot(data[:,0])
        plt.xlabel("Sample")
        plt.ylabel("Values")
        plt.grid(True)
        plt.tight_layout()
        plt.draw()
        plt.pause(.1)

        pltgif.append(plt)

    plt.clf()
    pltgif.save()
    tspsave('datal', f=fdim, d=ddim)
    plt.plot(fdim, ddim)
    plt.xlabel("Frequency(Hz)")
    plt.ylabel("Amplitude")
    plt.grid(True)
    plt.tight_layout()
    plt.show()
```
#### <font  color=#e06030>(2) 测量过程波形</font>
<p align="center"><img src="https://img-blog.csdnimg.cn/4962325e4d154d95b3de3aa1c9a80b01.gif" width="560" /><div align="center">▲ 图2.1.1  不同频率对应的波形</div></p><br>

#### <font  color=#e06030>(3) 测量结果</font>
<p align="center"><img src="https://img-blog.csdnimg.cn/e1e276ffad824c11a478b109c2ba2281.png" width="640" /><div align="center">▲ 图2.1.2 不同频率对应的信号的幅值</div></p><br>



&emsp;&emsp;可以看到在上面测量过程中，采集到的数据出现较大的问题，主要是出现数据的丢失。


### <font  color=#008090>2、测量高频截止频率</font>

&emsp; ●  **测量频率范围：** <br>
&emsp;&emsp;&emsp;``起始频率``：10kHz<br>
&emsp;&emsp;&emsp;``截止频率``：50kHz<br>
&emsp;&emsp;&emsp;``采集步数``：50<br>


#### <font  color=#e06030>(1) 采集到不同频率波形</font>

&emsp;&emsp;下面是测量过程中采集到的不同频率的波形：


<p align="center"><img src="https://img-blog.csdnimg.cn/91b6456242f94b65af4bc5aa08b42d8e.gif" width="560" /><div align="center">▲ 图2.1.3  测量过程不同波形</div></p><br>


#### <font  color=#e06030>(2) 幅频特性</font>

&emsp;&emsp;下面是不同频率下声卡采集到的信号波形的幅值。

<p align="center"><img src="https://img-blog.csdnimg.cn/14c7812a88bc4b018d387ad42fc21c87.png" width="640" /><div align="center">▲ 图2.1.4 不同频率下采集到的信号的幅值</div></p><br>


&emsp;&emsp;通过上述采集到的声卡的幅频特性，可以看到它的高频截止频率硬给了25kHz。


## <font  color=#7a37ab>三、设置不同采用频率</font>
### <font  color=#008090>1、提高采样频率</font>

&emsp;&emsp;设置高频采样频率为96kHz，重复上面测量高频截止频率过程。

<p align="center"><img src="https://img-blog.csdnimg.cn/5549151d315b4d83b731a4ef1f2c7aec.gif" width="560" /><div align="center">▲ 图2.3.1  采集过程不同频率对应的采集波形</div></p><br>



&emsp;&emsp;在96kHz下， 对应的声卡的幅频特性如下：


<p align="center"><img src="https://img-blog.csdnimg.cn/c190949711984ea6ae06573b4082db61.png" width="640" /><div align="center">▲ 图2.3.2 测量在采样频率96kHz下对应的信号的幅度</div></p><br>



&emsp;&emsp;可以看到提高采样频率并没有实质提高声卡的高频截止频率。

### <font  color=#008090>2、降低采样频率</font>

&emsp;&emsp;降低采样频率，设置声卡的采样频率为 24kHz。重新测试声卡的高频截止频率。

<p align="center"><img src="https://img-blog.csdnimg.cn/7f9d38b3cdc74597b2949ca95f790997.gif" width="560" /><div align="center">▲ 图2.3.3  采集到不同频率的信号波形</div></p><br>


<p align="center"><img src="https://img-blog.csdnimg.cn/21247b2440484a19babd4a67a4234786.png" width="640" /><div align="center">▲ 图2.3.4 声卡在采样频率为24kHz下的幅频特性</div></p><br>


&emsp;&emsp;从上面可以看到在24kHz 采样频率下，声卡的截止频率大约在12kHz。这说明在声卡前面已经有了抗混叠滤波器了。


&emsp;&emsp;下面是重新测量扫频范围在 10 ~ 20kHz下，采样频率在24kHz下对应的声卡的幅频特性。

<p align="center"><img src="https://img-blog.csdnimg.cn/9d2de5be86714aeeadefd1b3f473f732.png" width="640" /><div align="center">▲ 图2.3.5 声卡在采样频率24kHz下的幅频特性</div></p><br>



&emsp;&emsp;下面是设置采样频率为12kHz 下，声卡的高频部分的幅频特性。

<p align="center"><img src="https://img-blog.csdnimg.cn/bf96e8d021ef4176bbee506acd41c714.png" width="640" /><div align="center">▲ 图2.3.6 声卡在采样频率12kHz下的幅频特性</div></p><br>



&emsp;&emsp;通过上面测试结果可以看到，声卡的采样频率越低，对应的高频截止频率也越低。对应的截止频率（对应增益降低到原来的一半，-6dB）对应的频带宽度等于采样频率的一半。这证明了声卡中存在着前置的可编程的抗混叠滤波器。

### <font  color=#008090>3、测量低频截止频率</font>

&emsp;&emsp;设置声卡的采样频率为1200Hz，重新测量声卡的低频采样频率。

<p align="center"><img src="https://img-blog.csdnimg.cn/417919c6060249c8afc2c16873b2f438.gif" width="560" /><div align="center">▲ 图2.3.7  不同频率下采集到的波形</div></p><br>


<p align="center"><img src="https://img-blog.csdnimg.cn/f04dd918394b43e59a09de17994e6338.png" width="640" /><div align="center">▲ 图2.3.8 采样频率为1200Hz，低频下的幅频特性</div></p><br>


&emsp;&emsp;对比之前的结果来看，在低频阶段，声卡采集到的声音的波形依然存在着较大的损失。



### <font  color=#008090>4、测量中频段幅频特性</font>

&emsp;&emsp;测量1000 ~ 2000Hz中声卡的幅频特性。设置采样频率为24000Hz。

<p align="center"><img src="https://img-blog.csdnimg.cn/0fc8db087e484cd3a2f1ac1fbe7bd8b2.gif" width="560" /><div align="center">▲ 图2.3.9  不同频率采集到的波形</div></p><br>


<p align="center"><img src="https://img-blog.csdnimg.cn/64265b8e20a84d1c8f97e53097d7498a.png" width="640" /><div align="center">▲ 图2.3.10 采样频率为24000Hz下的声卡的幅频特性</div></p><br>


```python
from headm import *
import soundcard as sc
from tsmodule.tsvisa        import *

dg1062open(103)
dg1062freq(1,2000)

one_mic = sc.get_microphone('线路输入')

pltgif = PlotGIF()
with one_mic.recorder(samplerate=24000) as mic:
    mic.record(numframes=100000)

    fdim = linspace(1000, 2000, 50)
    ddim = []
    for f in fdim:
        dg1062freq(1,f)
        data = mic.record(numframes=1024)

        damp = max(list(data[:,0])) - min(list(data[:,0]))
        ddim.append(damp)

        printff(f, damp)

        plt.clf()
        plt.plot(data[:,0])
        plt.xlabel("Sample")
        plt.ylabel("Values")
        plt.grid(True)
        plt.tight_layout()
        plt.draw()
        plt.pause(.1)

        pltgif.append(plt)

    plt.clf()
    pltgif.save()
    tspsave('datal', f=fdim, d=ddim)
    plt.plot(fdim, ddim)
    plt.xlabel("Frequency(Hz)")
    plt.ylabel("Amplitude")
    plt.grid(True)
    plt.tight_layout()
    plt.show()
```



&nbsp;
<h1 align="center"> <center><font  face=黑体 color=slateblue size=6>※ <font color=red>实</font>验总结 ※</font></center> </h1>

---




&emsp;&emsp;<font color=gray size=5>测</font>试了python中soundcard软件包对于计算机声卡的操作，利用线路输入可以获取外部模拟信号的波形，并通过speaker进行播放。


&emsp;&emsp;通过程序，控制信号源DG1062，利用soundcard采集到信号波形，可以逐点获得声卡的幅频特性。声卡的高频截止频率最大为25kHz左右，不再随着采样频率的提高而提高。如果声卡的采样频率低于48000Hz，声卡中具有前端的抗混叠滤波器，抗混叠滤波器的截止频率等于采样频率的一半。


&emsp;&emsp;对于声卡的低频以及中频的幅频特性的测量，显示了采集过程中数据的不稳定性，这也实测测量结果出现了较大的抖动。


&emsp;&emsp;具体为什么使用soundcard中的 record 指令所获得数据出现截断过程，具体原因还不可而知。

 


<font face=宋体 color=gray>**■ 相关文献链接:**</font>
- [ 测试计算机声卡双通道录音的频率特性 ](https://zhuoqing.blog.csdn.net/article/details/120874310)
- [ 双声道录制的混动波形信号 ](https://zhuoqing.blog.csdn.net/article/details/120852245)
- [ 两个晶体管组成的混沌电路 ](https://zhuoqing.blog.csdn.net/article/details/120557306)
- [ PySoundCard ](https://pypi.org/project/PySoundCard/)
- [ SoundCard 0.4.1 ](https://pypi.org/project/SoundCard/)
- [ DG1062可编程信号源 ](https://zhuoqing.blog.csdn.net/article/details/110824422)


<font face=宋体 color=gray>**● 相关图表链接:**</font>
* [图1.3.1 显示采集到的正弦波波形](#987000)
* [图1.3.2 显示采集到的正弦波波形](#987001)
* [图1.3.3 显示采集到的1kHz的声音波形](#987002)
* [图2.1.1  不同频率对应的波形](#987003)
* [图2.1.2 不同频率对应的信号的幅值](#987004)
* [图2.1.3  测量过程不同波形](#987005)
* [图2.1.4 不同频率下采集到的信号的幅值](#987006)
* [图2.3.1  采集过程不同频率对应的采集波形](#987007)
* [图2.3.2 测量在采样频率96kHz下对应的信号的幅度](#987008)
* [图2.3.3  采集到不同频率的信号波形](#987009)
* [图2.3.4 声卡在采样频率为24kHz下的幅频特性](#987010)
* [图2.3.5 声卡在采样频率24kHz下的幅频特性](#987011)
* [图2.3.6 声卡在采样频率12kHz下的幅频特性](#987012)
* [图2.3.7  不同频率下采集到的波形](#987013)
* [图2.3.8 采样频率为1200Hz，低频下的幅频特性](#987014)
* [图2.3.9  不同频率采集到的波形](#987015)
* [图2.3.10 采样频率为24000Hz下的声卡的幅频特性](#987016)



```python
#!/usr/local/bin/python
# -*- coding: gbk -*-
#******************************
# TEST1.PY                     -- by Dr. ZhuoQing 2021-10-20
#
# Note:
#******************************
from headm import *
import soundcard as sc
from tsmodule.tsvisa        import *
dg1062open(103)
dg1062freq(1,2000)
one_mic = sc.get_microphone('线路输入')
pltgif = PlotGIF()
with one_mic.recorder(samplerate=24000) as mic:
    mic.record(numframes=100000)
    fdim = linspace(1000, 2000, 50)
    ddim = []
    for f in fdim:
        dg1062freq(1,f)
        data = mic.record(numframes=1024)
        damp = max(list(data[:,0])) - min(list(data[:,0]))
        ddim.append(damp)
        printff(f, damp)
        plt.clf()
        plt.plot(data[:,0])
        plt.xlabel("Sample")
        plt.ylabel("Values")
        plt.grid(True)
        plt.tight_layout()
        plt.draw()
        plt.pause(.1)
        pltgif.append(plt)
    plt.clf()
    pltgif.save()
    tspsave('datal', f=fdim, d=ddim)
    plt.plot(fdim, ddim)
    plt.xlabel("Frequency(Hz)")
    plt.ylabel("Amplitude")
    plt.grid(True)
    plt.tight_layout()
    plt.show()
#    data = mic.record(numframes=1024)
#    plt.plot(data)
#    plt.xlabel("Samples")
#    plt.ylabel("Values")
#    plt.grid(True)
#    plt.tight_layout()
#    plt.show()
#------------------------------------------------------------
#        END OF FILE : TEST1.PY
#******************************
```



★ 本文来自于CSDN文献：[**使用soundcard在Python中操作声卡**](https://zhuoqing.blog.csdn.net/article/details/120875107) 。
<br>
