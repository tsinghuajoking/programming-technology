<h1 align="center"> 在图片中如何生成带有文字边缘空心字体？ </h1>
<br>


<p align="center"><img src="https://img-blog.csdnimg.cn/5662a24ed30e4722a9d5c1b38b29b68b.gif" width="800" /></p>



><font  face=黑体 color=purple size=4>简 介：</font> 带有边缘的空心字体被广泛使用在各类视频的字母显示中，本文给出了一种简单的通过字体震动显示的方式产生空心字体的方法。这种方法可以在原来任何字体都生成相对应的空心字体。
<br><br><font color=slateblue face=宋体>**``关键词``**：</font> **空心字体**，**字幕**



&nbsp;
<h1 align="center"> §<center><font  face=黑体 color=slateblue size=6><u>01</u> <font color=red>空</font>心字体</font></center> </h1>

---



## <font  face=黑体 color=#7a37ab>一、为什么需要空心字体？</font>

在制作视频节目的过程中，为了能够在屏幕下面叠加字幕，需要以下几种模式：

* **模式1：**  直接叠加文字字幕；
* **模式2：** 字幕文字具有填充颜色；
* **模式3：** 字母横条；
* **模式4：** 字幕字体具有边缘；

如果统计在头条上的视频字幕形式，其中 具有白色（浅色）文字具有黑色（深色）变换字母形式居多（也就是**模式4**）。这种方式具有以下优点：
* 能够适应于以动态图像居多的画面中；比起模式1，它在背景画面中的无论什么形式都能够比较好的融合，很清晰的显示。
* 比起模式 2， 3来说，它对于画面的影响小。

因此，在图片中生成这类带有文字边缘的方法对于叠加字幕，特别是 [**TEASOFT教学软件**](https://zhuoqing.blog.csdn.net/article/details/107465301) 中自动生成字幕非常重要。


<p align="center"><img src="https://img-blog.csdnimg.cn/095b94b635714b8280a4fec66bd66264.png" width="560" /><div align="center">▲ 图1.1 在头条上不同视频的字母形式</div></p><br>




## <font  face=黑体 color=#7a37ab>二、如何生成空心字体？</font>
### <font  face=宋体 color=teal>1、选择空心字体</font>


在普通的字体中，绝大部分是没有这种带有文字边缘的字体，即所谓 的空心字体。

<p align="center"><img src="https://img-blog.csdnimg.cn/20e2962553354547934d981af63be5f3.gif" width="560" /><div align="center">▲ 图1.1.1  不同的字体</div></p><br>



在常用的Windows下的**华文彩云**字体具有这类空心字体，但这是这类文字在实际的字幕中使用不多。

<p align="center"><img src="https://img-blog.csdnimg.cn/09410cce681949d887d356336ab36900.png" width="320" /><div align="center">▲ 图1.1.2 华文彩云字体</div></p><br>




在 [**空心字体**](http://www.zhaozi.cn/s/61/) 网站可以下载更多的空心字体。



### <font  face=宋体 color=teal>2、生成字体边缘</font>
在 [**文字边缘轮廓制作教程-百度经验**](https://jingyan.baidu.com/article/72ee561a1b748de16138df27.html) 给出了使用PS制作文字边缘的方法。这种方法只适合于对于少量静态文字生成带有边缘的文字，不适合于编程实现。

下面介绍一种产生文字边缘的简单方法 - **文字抖动方法**。

#### <font  face=宋体 color=purple>(1) 基本原理</font>

文字抖动方法产生带有边缘文字原理很简单：就是首先使用边缘颜色，比如黑色，将文字在画面中进行显示，显示的位置往左右上下移动，它们叠加之后形成了原来文字加粗的形式。然后在将原来文字显示以白色显示在原来的位置。叠加之后便形成了带有边缘的字体了。

**文字抖动**方法就是指将文字上下左右位移叠加显示形成加速黑色背景的过程。

#### <font  face=宋体 color=purple>(2) 实验结果</font>
下面分别是将测试文字上下左右各分别移动1,2之后叠加的结果。

<p align="center"><img src="https://img-blog.csdnimg.cn/04c207e279594dea81dfb4e39327cc87.png" width="560" /><div align="center">▲ 图1.2.2 移动原来文字进行叠加显示形成加粗的形式</div></p><br>




<p align="center"><img src="https://img-blog.csdnimg.cn/498a088196af40cd8c380b6c76fdc075.png" width="560" /><div align="center">▲ 图1.2.2 移动原来文字进行叠加显示形成加粗的形式<br>移动距离为2</div></p><br>




然后在将原来的文字使用白色进行显示，叠加在原来加粗文字的上面，于是便形成了带有边缘的字体了。

下面是分别移动1,2之后叠加出来的效果。

<p align="center"><img src="https://img-blog.csdnimg.cn/6e0afea296dc4145a3dd3e8d3147de3a.png" width="560" /><div align="center">▲ 图1.2.3 叠加之后的效果</div></p><br>



<p align="center"><img src="https://img-blog.csdnimg.cn/e7354eb37acc404581658969d4d0490e.png" width="560" /><div align="center">▲ 图1.2.4 叠加之后的效果</div></p><br>




## <font  face=黑体 color=#7a37ab>三、阴影空心文字</font>

如果在原来上下左右移动 1 的情况下，再叠加一个往右下方移动 2 的阴影，则可以产生具有阴影的空心文字效果。

具体结果如下图所示：

<p align="center"><img src="https://img-blog.csdnimg.cn/eb7f415f6bc740869231545aa9532870.png" width="560" /><div align="center">▲ 图1.3.1 带有阴影边缘空心文字</div></p><br>




&nbsp;
<h1 align="center"> <center><font  face=黑体 color=slateblue size=6>※ <font color=red>测</font>试结论 ※</font></center> </h1>

---



带有边缘的空心字体被广泛使用在各类视频的字母显示中，本文给出了一种简单的通过字体震动显示的方式产生空心字体的方法。这种方法可以在原来任何字体都生成相对应的空心字体。


<p align="center"><img src="https://img-blog.csdnimg.cn/cfda368bc2754c29acd1dcf1a1c7e0a5.png" width="560" /><div align="center">▲ 图2.1.1 楷体字体空心文字</div></p><br>





<p align="center"><img src="https://img-blog.csdnimg.cn/eae9025938fd4410848a43d039ad4eea.png" width="560" /><div align="center">▲ 图2.1.2 宋体字体空心文字</div></p><br>




<p align="center"><img src="https://img-blog.csdnimg.cn/44350a01e97b4647a1311639e971b61a.png" width="560" /><div align="center">▲ 图2.1.3 华文行楷字体空心文字</div></p><br>






<p align="center"><img src="https://img-blog.csdnimg.cn/9b7fc5d6864a417f8909777bdb386cd3.png" width="560" /><div align="center">▲ 图2.1.5 中心颜色为黄色的空心字体</div></p><br>





最终这种方法在 TEASOFT 软件中得到了应用。

<p align="center"><img src="https://img-blog.csdnimg.cn/518a735699cf4cb38b0b691aad300242.gif" width="560" /></p>



<p align="center"><img src="https://img-blog.csdnimg.cn/17db399c22d546168fddd53e3c635d7f.png" width="800" /></p>


<p align="center"><img src="https://img-blog.csdnimg.cn/d44ae02d5de748f68fc6caba79cfa971.gif" width="560" /></p>





## <font  face=黑体 color=#7a37ab>■ 附件</font>


```python
#!/usr/local/bin/python
# -*- coding: gbk -*-
#******************************
# TEST1.PY                     -- by Dr. ZhuoQing 2021-09-11
#
# Note:
#******************************
from head import *
textid = [4, 3, 5, 6]
originid = 2
whiteid = 15
text1id = [16, 17, 18, 19]
range = tspgetdoplocation([4,3,5,6])
printf(range)
x = 2100
y = 1600
n=2
tspsetdopxy(4, x+n, y)
tspsetdopxy(3, x-n, y)
tspsetdopxy(5, x, y+n)
tspsetdopxy(6, x, y-n)
tspsetdopxy(16, x+n, y-n)
tspsetdopxy(17, x-n, y+n)
tspsetdopxy(18, x+n, y+n)
tspsetdopxy(19, x-n, y-n)
tspsetdopxy(whiteid, x, y)
tsprv()
printf("\a")
#------------------------------------------------------------
#        END OF FILE : TEST1.PY
#******************************
```


<font face=宋体 color=gray>**■ 相关文献链接:**</font>
- [ TEASOFT教学软件 ](https://zhuoqing.blog.csdn.net/article/details/107465301)
- [ 文字边缘轮廓制作教程-百度经验 ](https://jingyan.baidu.com/article/72ee561a1b748de16138df27.html)


<font face=宋体 color=gray>**● 相关图表链接:**</font>
* [图1.1 在头条上不同视频的字母形式](#987000)
* [图1.1.1  不同的字体](#987001)
* [图1.1.2 华文彩云字体](#987002)
* [图1.2.2 移动原来文字进行叠加显示形成加粗的形式](#987003)
* [图1.2.2 移动原来文字进行叠加显示形成加粗的形式<br>移动距离为2](#987004)
* [图1.2.3 叠加之后的效果](#987005)
* [图1.2.4 叠加之后的效果](#987006)
* [图1.3.1 带有阴影边缘空心文字](#987007)
* [图2.1.1 楷体字体空心文字](#987008)
* [图2.1.2 宋体字体空心文字](#987009)
* [图2.1.3 华文行楷字体空心文字](#987010)
* [图2.1.5 中心颜色为黄色的空心字体](#987011)


## <font  face=黑体 color=slateblue>◎ 公众号留言：</font>
- **``Master``** ：卓老师可以分享下TEASOFT软件么？
	* **作者：** 链接: https://pan.baidu.com/s/1cxTNcy9PjpXpc7VR9N1vkw 提取码: qczu 复制这段内容后打开百度网盘手机App，操作更方便哦 --来自百度网盘超级会员v5的分享
- **``最后一片傅里叶``** ：懂了 卓大大的意思是说 17届的赛道会是一个空心字
- **``小朋友``** ：有PS我不用，非要自己写算法，哎，就是玩
- **``梁飘จุ๊บ``** ：卓老师可以分享下TEASOFT软件么？
	* **作者：** 链接: https://pan.baidu.com/s/1cxTNcy9PjpXpc7VR9N1vkw 提取码: qczu 复制这段内容后打开百度网盘手机App，操作更方便哦 --来自百度网盘超级会员v5的分享
- **``周琛``** ：说有PS我不用的，做一幅图可以用PS，做一个几小时纪录片的字幕还能用PS吗？
- **``不瘦20斤不改网名``** ：懂了下届赛道要会认空心字，看路牌跑了
- **``Pacino``** ：卓老师可以分享下TEASOFT软件么？
- **``Obscure``** ：用arctime做字幕可以调白边大小



● 本文来自于CSDN文献：<https://zhuoqing.blog.csdn.net/article/details/120228492>
