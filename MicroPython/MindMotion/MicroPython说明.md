

<br><h1 align="center"> <center><font  face=黑体 color=slateblue size=6>§<u>01</u> <font color=red>S</font>uYong版本</font></center> </h1>

---

## <font  color=#7a37ab>一、调试记录</font>

* [**测试MM32F3277-MicroPython 2021-11-17 版本**](https://zhuoqing.blog.csdn.net/article/details/121372618) 
* [**设计带有SD卡的 MM32F3277 MicroPython 实验板**](https://zhuoqing.blog.csdn.net/article/details/121174772) 
* [**MM32F3277 MicroPython移植过程中对应的接口文件**](https://zhuoqing.blog.csdn.net/article/details/121160409) 
* [**在MM32F3273上运行MicroPython，对于性能进行测试**](https://zhuoqing.blog.csdn.net/article/details/121178770) 
* [**基于MM32F3273的MicroPython实验电路板 - 工作并不是正常**](https://zhuoqing.blog.csdn.net/article/details/121193825) 



<br><h1 align="center"> <center><font  face=黑体 color=slateblue size=6>§<u>02</u> <font color=red>S</font>eekFree版本</font></center> </h1>

---

## <font  color=#7a37ab>一、调试记录</font>


*  [**调试来自于逐飞的MM32F3277移植有MicroPython开发板**](https://zhuoqing.blog.csdn.net/article/details/120997254) 
*  [**测试逐飞的MM32F3277 MicroPython开发板的基本功能**](https://zhuoqing.blog.csdn.net/article/details/121010813) 