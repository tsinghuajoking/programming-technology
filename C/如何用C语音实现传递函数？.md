<h1 align="center"> 如何用C语音实现传递函数？ </h1>
<br>


<p align="center"><img src="https://img-blog.csdnimg.cn/2e740995db4c4fb4b66fa3a0e6c70c2b.png" width="800" /></p>


><font  face=黑体 color=purple size=4>简 介：</font> 根据学生提出的如何使用C语言来实现系统函数的问题，给出了利用MATLAB，Python对连续时间系统函数进行离散化的方法。继而可以进行C语言实现。
<br><br><font color=slateblue face=宋体>**``关键词``**：</font> **系统函数**，**离散化**，**MATLAB**



&nbsp;
<h1 align="center"> §<center><font  face=黑体 color=slateblue size=6><u>01</u> <font color=red>如</font>何实现传递函数？</font></center> </h1>

---




&emsp;&emsp;<font color=gray size=5>卓</font>晴老师，请教一下，传递函数环节用``C``需要编写，该怎么编写？在``matlab``中他能简单的表述，但是在利用``C``语言，该咋办呢？


&emsp;&emsp;例如写进单片机中。类似于写一个传递函数通式，想实现各种环节。

<p align="center"><img src="https://img-blog.csdnimg.cn/a1ef1b1c0f6e4de5b29639f19d90bfd2.png" width="560" /></p>



&emsp;&emsp;在``matlab``中他有很好的封装，却不好理解``matlab``怎么做到的，``simulink``中一个时域信号输入传递模块输出还是时域信号，逐步仿真我认为这个传递模块中封装着一个复杂表达式，想知道用``C``语言咋能实现，这样以后对信号处理想加个滤波环节，一阶延时，可以利用这种方法直接解决。


&emsp;&emsp;好的，感谢卓大大，最好给出一个``G/TS+1``这种的一个``C``代码的例子，然后我就可以仿照思路写出更多高阶的。


&nbsp;
<h1 align="center"> §<center><font  face=黑体 color=slateblue size=6><u>02</u> <font color=red>什</font>么是传递函数？</font></center> </h1>

---


&emsp;&emsp;<font color=gray size=5>要</font>回答前面同学提出的“**如何使用C编程语言实现传递函数？**”，则需要先了解什么是系统传递函数？为什么引入系统传递函数？

## <font  color=#7a37ab>一、线性时不变系统</font>

&emsp;&emsp;下面两个由线性R，C组成的电路网络，它们的共同之处都属于 [**线性时不变系统**](https://baike.baidu.com/item/%E7%BA%BF%E6%80%A7%E6%97%B6%E4%B8%8D%E5%8F%98%E7%B3%BB%E7%BB%9F/2474061) ，不同之处在于左边的电路输入输出之间是一个比例关系，$${{U_O \left( t \right)} \over {U_I \left( t \right)}} = {{R_{12} } \over {R_{11}  + R_{12} }}$$


&emsp;&emsp;这类系统被称为**即时系统**。

<p align="center"><img src="https://img-blog.csdnimg.cn/ebd5fe067ab449bebc28198b29227781.png" width="560" /><div align="center">▲ 图2.1.1 左：电阻分压电路；右：阻容低通滤波电路</div></p><br>


&emsp;&emsp;右边的低通滤波电路输入输出之间关系则需要通过微分方程才能够描述：$$R_{21} C_{11}  \cdot {{dV_O \left( t \right)} \over {dt}} + V_O \left( t \right) = V_I \left( t \right)$$


&emsp;&emsp;这种带有储能器件的电路，当前的输出不仅与当前的输入有关系，还和之前时间输入有关系。这类系统被称为**动态系统**，显然动态系统比即使系统复杂，但功能更强大。即使系统可以看成一类特殊的动态系统。

## <font  color=#7a37ab>二、系统函数</font>



&emsp;&emsp;既然两个电路都属于线性时不变系统，可否将它们的输入输出关系都简化成比例关系，这样分析与求解就比较方便了？


&emsp;&emsp;答案是可以的，只不过需要把输入输出信号都进行某种变换，比如 [**傅里叶变换**](https://mathworld.wolfram.com/FourierTransform.html#:<sub>:text=The%20smoother%20a%20function%20%28i.e.%2C%20the%20larger%20the,Fourier%20transform%20is%20also%20symmetric%20since%20implies%20.) ， [**拉普拉斯变换**](https://byjus.com/maths/laplace-transform/#:</sub>:text=The%20Laplace%20transform%20is%20a%20well%20established%20mathematical,to%20calculate%20the%20solution%20to%20the%20given%20problem.) ，这时变换后的系统输入输出之间就是一个比例关系了。这个比值就被称为线性时不变分析对象的**系统函数**。


&emsp;&emsp;在拉普拉斯变换下，上面的电阻分压电路对应的系统函数为：$$F_1 \left( s \right) = {{U_O \left( s \right)} \over {U_I \left( s \right)}} = {{R_{12} } \over {R_{11}  + R_{12} }}$$


&emsp;&emsp;阻容低通滤波电路对应的系统函数为：
$$F_2 \left( s \right) = {{U_O \left( s \right)} \over {U_I \left( s \right)}} = {1 \over {R_{21} C_{11} s + 1}} = {1 \over {\tau  \cdot s + 1}},\space \space \space \space \space \tau  = R_{21} C_{11}$$



&emsp;&emsp;如果抛开这些变换概念背后的数学基础，从工程应用角度也可以将上述系统函数看成对描述线性时不变系统输入输出关系之间的微分方程的另外一种表示方式。


&emsp;&emsp;借助于系统函数的概念，就把线性时不变系统的分析、设计都可以进行简化。


<p align="center"><img src="https://img-blog.csdnimg.cn/e04de81495a0473fbe1cccfb17bc296c.png" width="560" /><div align="center">▲ 图2.2.1 在变换域内定义的系统函数等于系统的零状态响应与输入信号的比值</div></p><br>



## <font  color=#7a37ab>三、离散时间系统函数</font>


&emsp;&emsp;计算机的普及使得离散时间系统应用增加，系统可以通过软件编程来实现。如果实现对于输入信号进行低通滤波，也可以在对信号进行采样之后利用下面离散时间系统来进行处理。


<p align="center"><img src="https://img-blog.csdnimg.cn/92d7aa60bf2f48f4b234cafc793a1db1.png" width="560" /><div align="center">▲ 图2.3.1.1 一阶低通离散时间系统</div></p><br>



&emsp;&emsp;下图中显示的随机噪声的原始信号，经过上面迭代方程滤波之后，就会形成平滑后的数据。其中的滤波器系数$a = 0.99$。

<p align="center"><img src="https://img-blog.csdnimg.cn/b4611ca2fb19461ca5fed06e2d2221de.png" width="560" /><div align="center">▲ 图2.3.3 增加有噪声信号以及一阶低通滤波后的信号</div></p><br>

```python
from headm import *

t = linspace(0, 10, 1000)
sint = sin(t)

data = sint + random.rand(len(t))

def filter_1(data, a):
    yn = data[0]
    newd = [yn]

    for d in data[1:]:
        yn = d*(1-a) + a * yn
        newd.append(yn)

    return newd

data_filter = filter_1(data, 0.99)

plt.plot(t, data, label='origin')
plt.plot(t, data_filter, label='filter data')
plt.xlabel("t")
plt.ylabel("data")
plt.grid(True)
plt.legend(loc='upper right')
plt.tight_layout()
plt.show()
```





&emsp;&emsp;线性时不变动态离散时间系统可以使用差分方程来刻画，经过简单的一项便可成写出的迭代方程形式，即由输入信号以及以前的输出信号计算出当前系统输出值，这是利用软件编程实现的基础。


&emsp;&emsp;同样，在对于系统的输入输出信号进行 [**Z-变换**](https://faculty.ksu.edu.sa/sites/default/files/dsp_cen352_ch5_z.pdf#:~:text=Definition%20of%20z-transform%3A%20The%20z-transform%20is%20a%20very,filter%20design%20and%20frequency%20analysis%20of%20digital%20signals.) ，那么系统的零状态输出信号比上输入信号也是一个常量，定义为离散时间系统的系统函数。上述一阶低通离散时间系统对应的系统函数为：$$H\left( z \right) = {{1 - a} \over {1 - az^{ - 1} }} = {{\left( {1 - a} \right)z} \over {z - a}}$$


&emsp;&emsp;同样，如果抛开z变换背后的数学原理，仅仅把$z^{ - 1}$看成对于信号的延迟操作，那么上面的系统函数也可以看成差分方程另外一种描述方法。可以看到系统函数所对应的有理分式的分子多项式系数对应着差分方程输入信号前的系数；分母多项式的系数对应差分方程输出信号前的系数。

<p align="center"><img src="https://img-blog.csdnimg.cn/0c39eff5520d4c3e8860814b7e45a235.png" width="560" /><div align="center">▲ 图2.3.2 离散时间系统的系统方程</div></p><br>




&emsp;&emsp;因此，如果已知知道了离散时间系统函数，则按照上面的系统函数与差分方程对应关系可以写出对应的差分方程，进而整理成对应的迭代方程。这样就可以使用编程语言（C,Python）来实现算法了。




&nbsp;
<h1 align="center"> §<center><font  face=黑体 color=slateblue size=6><u>03</u> <font color=red>如</font>何将H(s)转换成H(z)?</font></center> </h1>

---




&emsp;&emsp;<font color=gray size=5>本</font>文一开始同学询问，如果知道了一个连续时间系统的传递函数，怎么使用C语言编程实现系统功能？这个问题就转换成如何将连续时间系统函数$H\left( s \right)$转换成对应的离散时间系统函数$H\left( z \right)$的问题了。这个过程也称为连续时间系统的离散化。

## <font  color=#7a37ab>一、连续系统函数离散化</font>

### <font  color=#008090>1、离散化方法</font>


&emsp;&emsp;将连续时间系统转换成对应的离散时间系统的过程常常用在**模拟设计-离散实现**的设计过程中。通常使用的方法包括：
* 冲激响应不变法：这种方法可以保持离散时间系统的单位冲激响应是对应连续时间系统的采样；
* 变量替换法：包括前向差分（Euler方法）、后向差分、双线性法，也称梯形方法；


&emsp;&emsp;除此之外，在 [**Continuous**](https://ww2.mathworks.cn/help/control/ug/continuous-discrete-conversion-methods.html) 中给出了在控制系统中常用到的六种方法及其转换原理。

### <font  color=#008090>2、举例</font>


&emsp;&emsp;比如使用双线性方法，它是基于把代表位移算子的$z$所对应的连续系统函数近似展开成有理分式：
$$z = e^{sT_s }  \approx {{1 + sT_s /2} \over {1 - sT_s /2}},\space \space \space s = {2 \over {T_s }} \cdot {{z - 1} \over {z + 1}}$$



&emsp;&emsp;以一个一阶低通滤波器为例：$$H\left( s \right) = {G \over {Ts + 1}}$$


&emsp;&emsp;使用双线性方法转换成离散系统函数：
$$H\left( z \right) = {G \over {{{2T} \over {T_s }} \cdot {{z - 1} \over {z + 1}} + 1}} = {{G\left( {1 + z<sup>{ - 1} } \right)} \over {\left( {{{2T} \over {T_s }} + 1} \right) - {{2T} \over {T_s }}z</sup>{ - 1} }}$$

## <font  color=#7a37ab>二、使用MATLAB离散化系统函数</font>

&emsp;&emsp;利用MATLAB内部函数  [**c2d**](https://ww2.mathworks.cn/help/control/ref/lti.c2d.html#mw_53fc4689-2099-41d0-93b3-de1e51a174c1) ，可以很方便将连续时间系统函数转换成对应的离散时间系统函数。

### <font  color=#008090>1、命令语法</font>

&emsp;&emsp;MATLAB 中给出了一下四种常用到离散系统函数命令形式：

```python
sysd = c2d(sysc,Ts)
sysd = c2d(sysc,Ts,method)
sysd = c2d(sysc,Ts,opts)
[sysd,G] = c2d(___)
```

&emsp;&emsp;第一种方法利用了零阶保持方法完成系统函数离散化，如果希望指明离散化具体方法，可以使用2,3两种命令形式。



### <font  color=#008090>1、转换举例</font>
#### <font  color=#e06030>（1）连续时间系统函数</font>


&emsp;&emsp;将如下带有延时环节的二阶连续时间系统进行离散化：

$$H\left( s \right) = e<sup>{ - 0.25s} {{10} \over {s</sup>2  + 3s + 10}}$$

#### <font  color=#e06030>（2）转换命令</font>
```python
h = tf(10,[1 3 10],'IODelay',0.25); 
hd = c2d(h,0.1)
```
```python
hd =
 
           0.01187 z^2 + 0.06408 z + 0.009721
  z^(-3) * ----------------------------------
                 z^2 - 1.655 z + 0.7408
 
Sample time: 0.1 seconds
Discrete-time transfer function.
```
#### <font  color=#e06030>（3）系统仿真</font>


&emsp;&emsp;通过以下指令，可以对比连续时间系统与离散时间系统单位冲激响应波形。


>step(h,'--',hd','-')


<p align="center"><img src="https://img-blog.csdnimg.cn/7e546ff0b83d4b9a87ac92d8b136a54a.png" width="560" /><div align="center">▲ 图3.2.2.1 系统的单位冲激响应信号对比</div></p><br>



&nbsp;
<h1 align="center"> <center><font  face=黑体 color=slateblue size=6>※ <font color=red>问</font>题总结 ※</font></center> </h1>

---



&emsp;&emsp;<font color=gray size=5>使</font>用嵌入式系统实现信号处理算法、控制算法，比起利用连续时间系统（或者模拟电路）实现具有很大的优点。在一些模拟设计（仿真）-数字化实现的场景中，将连续时间系统函数转换成对应的离散时间系统函数是编程实现控制算法的第一步。


&emsp;&emsp;离散化系统函数的方法有很多种，在MATLAB中可以利用相关的命令完成这些转换，并进行系统仿真测试。


&emsp;&emsp;手工实现连续系统传递函数的一般过程：
* 将连续时间系统函数转换成离散时间系统函数；
* 将离散时间系统函数整理成关于$z^{ - 1}$的有利多项式；
* 根据$z^{ - 1}$有理多项式写对应的的差分方程；
* 将差分方程整理成**迭代方程形式**，便可以通过C语言完成其中的运算了。


&emsp;&emsp;这个过程也可以借助于 [**MATLAB 代码转成C语言代码**](https://www.mathworks.com/videos/automatically-converting-matlab-code-to-c-code-96483.html) 的辅助功能来加速，甚至在一些硬件平台上，生成的C语言代码可以直接部署在嵌入式单片机中来执行。

<p align="center"><img src="https://img-blog.csdnimg.cn/a93e261a346c4320892bcce88c3af057.gif" width="800" /></p>






&emsp;&emsp;在 [**control.TransferFunction**](https://python-control.readthedocs.io/en/0.8.3/generated/control.TransferFunction.html) 中给出了利用 Python 中的 control 开发包进行连续时间系统离散化相关指令。



&emsp;&emsp;下面示例给出了给出了将系统函数：$H\left( s \right) = {1 \over {s + 1}}$转换成对应的离散时间系统函数：$$H\left( z \right) = {{0.2z + 0.2} \over {z - 0.6}},\space \space \space dt = 0.5$$


```python
import control
sys = control.TransferFunction(1,[1,1])
sysd = sys.sample(0.5, method='bilinear')
```

 

 



<font face=宋体 color=gray>**■ 相关文献链接:**</font>
- [ 线性时不变系统 ](https://baike.baidu.com/item/%E7%BA%BF%E6%80%A7%E6%97%B6%E4%B8%8D%E5%8F%98%E7%B3%BB%E7%BB%9F/2474061)
- [ 傅里叶变换 ](https://mathworld.wolfram.com/FourierTransform.html#:~:text=The%20smoother%20a%20function%20%28i.e.%2C%20the%20larger%20the,Fourier%20transform%20is%20also%20symmetric%20since%20implies%20.)
- [ 拉普拉斯变换 ](https://byjus.com/maths/laplace-transform/#:~:text=The%20Laplace%20transform%20is%20a%20well%20established%20mathematical,to%20calculate%20the%20solution%20to%20the%20given%20problem.)
- [ Z-变换 ](https://faculty.ksu.edu.sa/sites/default/files/dsp_cen352_ch5_z.pdf#:~:text=Definition%20of%20z-transform%3A%20The%20z-transform%20is%20a%20very,filter%20design%20and%20frequency%20analysis%20of%20digital%20signals.)
- [ Continuous ](https://ww2.mathworks.cn/help/control/ug/continuous-discrete-conversion-methods.html)
- [ c2d ](https://ww2.mathworks.cn/help/control/ref/lti.c2d.html#mw_53fc4689-2099-41d0-93b3-de1e51a174c1)
- [ MATLAB 代码转成C语言代码 ](https://www.mathworks.com/videos/automatically-converting-matlab-code-to-c-code-96483.html)
- [ control.TransferFunction ](https://python-control.readthedocs.io/en/0.8.3/generated/control.TransferFunction.html)


<font face=宋体 color=gray>**● 相关图表链接:**</font>
* [图2.1.1 左：电阻分压电路；右：阻容低通滤波电路](#987000)
* [图2.2.1 在变换域内定义的系统函数等于系统的零状态响应与输入信号的比值](#987001)
* [图2.3.1.1 一阶低通离散时间系统](#987002)
* [图2.3.3 增加有噪声信号以及一阶低通滤波后的信号](#987003)
* [图2.3.2 离散时间系统的系统方程](#987004)
* [图3.2.2.1 系统的单位冲激响应信号对比](#987005)




★ 本文来自于CSDN文献：[**如何用C语音实现传递函数？**](https://zhuoqing.blog.csdn.net/article/details/120540191) 。
<br>
