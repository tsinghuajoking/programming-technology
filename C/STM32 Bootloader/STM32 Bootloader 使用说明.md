<p align="center"><img src="https://img-blog.csdnimg.cn/2f21960d9b0043b0b0064133ae77ec90.png" width="800" />


<h1 align="center"> <center><font  face=黑体 color=slateblue size=6>§<u>01</u> <font color=red>S</font>TM32</font></center> </h1>

---

## <font  color=#944276>一、软件功能</font>

STM32 Bootloader 是用于对STM32以及兼容单片机进行程序下载的界面。它通过STM32的 UART Bootloader协议将执行程序的HEX文件下载到STM32 Flash中，并进行执行。由于它通过串口下载，所以在程序执行之后，便可以动态接收到单片机发送UART的信息，这便于进行程序的开发。

STM32也可以通过WiFi组成的无线串口完成程序的下载

在“ **Return** ” 被选中之后， STM32也可以形成一个基于串口的交互界面。用于MicroPython REPL交互。


<p align="center"><img src="https://img-blog.csdnimg.cn/7cb14b4cd19e404d9a30630527386022.png" width="560" /><div align="center">▲ 图1.1 STM32界面</div></p><br>



## <font  color=#944276>2、UDP命名集合</font>

STM32 启动之后，便通过本机端口  **2244**  接收UDP命令。下面将对应的命令以及相关的功能进行描述。

<div align="left"><font face=宋体 color=slateblue>【表1-2 UDP命令功能】</font></div>

<font face=宋体 color=slateblue size=3>UDP命令</font>|<font face=宋体 color=slateblue size=3>命令字符</font>|<font face=宋体 color=slateblue size=3>功能</font>
--|--|--
关闭串口|CLOSEPORT|将UART端口关闭掉。这个功能是针对于其它软件共同应用相同的串口。
打开串口|REOPENPORTFOCUS|重新打开UART端口，并停留在STM32软件界面。
重开串口|REOPENPORT|重新开启UART端口。
拷贝信息|COPY|将信息窗口中的字符拷贝到剪切板。
剪切信息|CUT|将信息窗口中的内容CUT到剪切板。
显示信息|SHOWINFOR|将随后的信息显示在信息窗口，并且换行。
设置信息|SETINFOR|将信息字符串显示在信息串口，不换行。
读取全局数据|METER|返回全局信息以及接收到的数据。
读取MEMO|MEMOL|读取信息窗口中的字符串。
读取MEMO信息|MEMOR| MEMOR START LENGTH
读取MEMO|MEMOS|将信息窗口内字符串读取。
清除信息|CLEAR|将信息窗口内信息清除。
读取字节|BYTES|读取接收到的ByteBuffer。
下载MicroPython|MPDLD|将全局剪切板上的信息下载到MicroPython REPL中。
发送剪切板|SENDP|将剪切板上的字符通过 PORT1发送。
发送字符|SENDC|将SENDC后面跟着的字符发送。


```python
ClearInfor();
strcpy(szString, "");
Clipboard()->GetTextBuf(szString, sizeof(szString) - 1);
MessageBeep(0);
SendChar(0x5, PORT1);       // Send CTRL+A
int nLength = strlen(szString);
int i;
for(i = 0; i < nLength; i ++)
    SendChar(szString[i], PORT1);

SendChar(0x4, PORT1);      // Send CTRL+B
```











